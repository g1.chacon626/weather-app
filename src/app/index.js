const { Weather } = require('./Weather');
const { UI } = require('./Ui');
const { Store } = require('./Store');

const store = new Store();
const {city , countryCode} = store.getlocationData();
const ui = new UI();
const weather = new Weather(city , countryCode);

require('./index.css');
async function fetchWeather() {
    const data = await weather.getWeather();
    console.log(data);
    ui.render(data);
}
document.getElementById('w-change-btn').addEventListener('click',(e)=>{
        const city = document.getElementById('city').value;
        const countryCode = document.getElementById('country-code').value;
        weather.changeLocation(city,countryCode);
        store.setLocationData(city,countryCode);
        fetchWeather();
        e.preventDefault();
        console.log(city,countryCode);
})
document.addEventListener('DOMContentLoaded', fetchWeather);